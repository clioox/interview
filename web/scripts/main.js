/*
ToDO
    - hover effekt
*/

$(() => {
    $.ajax({
        url: `${window.location.origin}/post/all`,
        type : 'GET'
    }).done(( response ) => {
        let click_in_process = false;

        $('#searchSuggests').mousedown(() => {
            click_in_process = true;
        });

        $('#searchField').focus(() => {
            $('#searchSuggests').show();
        }).blur(() => {
            if(!click_in_process) {
                $('#searchSuggests').hide();
            }
        });

        $( "#searchField" ).keyup((e) => {
            $('#searchSuggests').html('');
            const { value } = e.target;
            const excludeColumns = ['id'];

            filteredData = response.filter(item => {
                return Object.keys(item).some(key => {
                    return (excludeColumns.includes(key) || item[key] == null) ? false : item[key].toLowerCase().includes(value);
                });
            });

            $('#searchSuggests').html(() => {
                return filteredData.map(item => {
                    return `<li><a href="/post/${item.id}">${item.title}</a></li>`;
                }).join('');
            });
        });
    }).fail(( response ) => {
        console.error(`Looks like there was a problem:\n${response.statusText}`);
    });
});